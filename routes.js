const routes = module.exports = require('next-routes')()

routes
    .add({ pattern: '/pantip_review', page: 'pantipReviewPage' })
    .add({ pattern: '/edit_tags', page: 'editTagsPage' })
    .add({ pattern: '(.*)', page: 'notfound' })