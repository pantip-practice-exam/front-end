import axios from 'axios'

export const fetchApiPantipReview = () => async dispatch => {

    dispatch({ type: 'FETCH_PENDING' })

    try {
        
        const pantipReview = await axios.get('http://www.mocky.io/v2/5e1d227c3600002a00c73e82')

        dispatch({
            type: 'FETCH_SUCCESS',
            payload: pantipReview.data
        })

    }
    catch (error) {

        dispatch({
            type: 'FETCH_FAILED',
            error
        })

    }

}



