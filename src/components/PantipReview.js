import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { fetchApiPantipReview } from '../actions/fetchApi'

const PantipReview = props => {

    const dispatch = useDispatch()

    const pantipReview = useSelector(state => state.fetchApiPantipReview.data)

    useEffect(() => {
        
        dispatch(fetchApiPantipReview())

    }, [])

    const randerItem = pantipReview => {

        return pantipReview.map((item, key) => {

            return (
                <li className="pt-list-item" key={key} >
                    <div className="pt-list-item__title" >
                        <h2 >
                            <a href={item.image_url}
                                target="_blank"
                                className="gtm-main-content-link" >
                                <i className="pantip-icons md-18 pt-ic-topic_type_review" />
                                {item.title}
                            </a>
                        </h2>
                        <span >
                            <a href={item.image_url}
                                target="_blank" />
                        </span>
                    </div>
                    <a href={item.image_url}
                        target="_blank" className="gtm-main-content-link" >
                        <div className="pt-list-item__img img-thumbnail"
                            style={
                                {
                                    backgroundImage: `url(${item.image_url})`
                                }
                            }
                        />
                    </a>
                    <div className="pt-list-item__info" >
                        <a href={item.image_url} >
                            <div className="img-thumbnail img-circle"
                                style={
                                    {
                                        backgroundImage: `url(${item.image_url})`
                                    }
                                } />
                        </a>
                        <h5 >
                            <a href={item.image_url} />
                        </h5>
                        <span />
                    </div>
                </li>
            )
        })

    }

    return (
        <div className="container" style={{marginTop : '100px'}}>
            <div className="pt-block-purple-2 m-b-20">
                <div className="pt-carousel">
                    <div className="pt-carousel-container">
                        <ul className="pt-list pt-list__type-b pt-list__title2line pt-list__type-b-opt4item_on_1row"
                            style={
                                { left: "calc(0%)" }} >
                            {
                                 pantipReview?.data && randerItem(pantipReview.data)
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PantipReview