// Libs
import { combineReducers } from 'redux'

// reducers
import fetchApiReducer from './fetchApiReducer'

export default combineReducers({
    fetchApiPantipReview : fetchApiReducer
})