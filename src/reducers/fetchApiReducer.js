
export const initialState = {
    isFetching: false,
    error: null,
    data: {}
}

export default (state = initialState, { type, payload, error }) => {
    switch (type) {
        case 'FETCH_PENDING':
            return {
                ...state,
                isFetching: true
            }
        case 'FETCH_SUCCESS':
            return {
                ...state,
                isFetching: false,
                data: payload
            }
        case 'FETCH_FAILED':
            return {
                ...state,
                isFetching: false,
                error,
                data: {}
            }
        default:
            return state
    }
}

