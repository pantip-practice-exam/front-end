import React from 'react'
import { Provider } from 'react-redux'
import createReduxStore from '../src/store'
import withRedux from 'next-redux-wrapper'

const ReduxStore = (initialState, option) => createReduxStore(initialState, option)

const MyApp = ({ Component, pageProps, store }) => {
    return (
        <Provider store={store}>
            <Component {...pageProps} />
        </Provider>
    )
}

MyApp.getInitialProps = async({ Component, ctx }) => {

    let pageProps = {}

    if (Component.getInitialProps) {
        pageProps = await Component.getInitialProps(ctx)
    }
    
    return { pageProps }
}

export default withRedux(ReduxStore)(MyApp)