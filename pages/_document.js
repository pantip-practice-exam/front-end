import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {

    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head>
                    <meta charSet='utf-8' />
                    <meta name='viewport' content='width=device, initial-scale=1, shrink-to-fit=no' />

                    <link rel="stylesheet" href="https://pantip.com/static/v1.6/main.css" />
                    <link rel="stylesheet" href="https://pantip.com/css/version/1584936201/css-tag2018.css" />
                </Head>
                <body>
                        <Main />
                        <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument