import React, { useState, useEffect } from 'react'
import axios from 'axios'
import _ from 'lodash'
const EditTagsPage = props => {

    const [tagsName, setTagsName] = useState('')
    const [tagsListTopic, setTagsListTopic] = useState([])
    const [topic, setTopic] = useState([])

    useEffect(() => {

        (async () => {
            let topicData = await axios.get('http://www.mocky.io/v2/5e1d24dd3600005a00c73e8c')
            topicData = topicData.data.data.length > 0 ? topicData.data.data : []
            setTopic(topicData)
        })()

    }, [])

    const listTopic = (topic, tagsListTopic) => {

        return topic.map((item, key) => {

            let conditon = _.difference(item.tag,tagsListTopic).length !== item.tag.length

            const classLi = conditon ? 'pt-list-item_interesting' : ''

            return (
                <li className={`pt-list-item pt-list-item__no-img ${classLi}`} key={key}>
                    <div className="pt-list-item__title">
                        <h2>{item.title}</h2>
                        <span>
                            <a
                                href="https://pantip.com/topic/39781836"
                                className="gtm-latest-topic gtm-topic-layout-compact gtm-topic-type-filter-all"
                            />
                        </span>
                        <div className="pt-list-item__tag">
                            {
                                item.tag.map((item, key) => {
                                    return (
                                        <a
                                            className="gtm-latest-topic gtm-topic-layout-compact gtm-topic-type-filter-all"
                                            href={`/tag/${item}`}
                                            target="_blank"
                                            key={key}
                                        >
                                            {item}
                                        </a>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <a
                        href="https://pantip.com/topic/39781836"
                        target="_blank"
                        className="gtm-latest-topic gtm-topic-layout-compact gtm-topic-type-filter-all"
                    >
                        <div
                            className="pt-list-item__img img-thumbnail"
                            style={{ backgroundImage: 'url("")' }}
                        />
                    </a>
                    <div className="pt-list-item__info">
                        <a
                            href="https://pantip.com/profile/5832635"
                            target="_blank"
                            className="gtm-latest-topic gtm-topic-layout-compact gtm-topic-type-filter-all"
                        >
                            <div
                                className="img-thumbnail img-circle"
                                style={{
                                    backgroundImage:
                                        "url(https://ptcdn.info/images/avatar_member_default.png)"
                                }}
                            />
                        </a>
                        <h5>{item.name}</h5>
                    </div>
                    <div className="pt-list-item__stats">
                        <span className="pt-li_stats-comment">
                            <i className="material-icons md-18">comment : {item.comment}</i>
                        </span>
                        <span className="pt-li_stats-vote">
                            <i className="material-icons md-18">vote : {item.vote}</i>
                        </span>
                    </div>
                </li>
            )
        })
    }

    return (
        <div className="container" style={{ marginTop: '100px' }}>

            <div className="tags-search-box">
                <span>Highligth Tag</span>
                <input type="text" className="tags-search-box-input" name="q" size="55" id="search-tag-text"
                    value={tagsName} onChange={(e) => setTagsName(e.target.value)}
                />
                <button type="button" className="btn btn-primary m-r-8" onClick={() =>  setTagsListTopic([...tagsListTopic, tagsName])} >SUBMIT</button>
            </div>


            <div className="col-lg-8" style={{ position: "static" }}>
                <div id="pt-topic-left" className="pt-block pt-block-purple-2 m-b-20">
                    <div id="pt-list-header_01_outer">
                        <div id="pt-list-header_01_inner"></div>
                        <div className="pt-block-header pt-block-header-purple-1">
                            <h6 className="txt-yellow-700">กระทู้ล่าสุด</h6>
                        </div>
                    </div>

                    <ul className="pt-list pt-list-item__full-title pt-list__type-a">
                        {
                            topic.length > 0 && listTopic(topic, tagsListTopic)
                        }
                    </ul>
                </div>
            </div>

        </div>
    )
}

export default EditTagsPage