// Libs
import React, { useEffect } from 'react'

// Components
import PantipReview from '../src/components/PantipReview'

const PantipReviewPage = props => {
    return (
        <div>
            <PantipReview />
        </div>
    )
}

export default PantipReviewPage